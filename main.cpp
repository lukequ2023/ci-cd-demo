#include <iostream>
#include "hello/hello.h"
#include "world/world.h"

int main()
{
  // std::cout << "hello world" << std::endl;
  Hello();
  World();
  return 0;
}
